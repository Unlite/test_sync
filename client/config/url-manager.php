<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 08.09.2019
 * Time: 18:13:25
 */

return [
	'class' => 'yii\web\UrlManager',
	'hostInfo' => $params['clientUrlManager'],
	'baseUrl' => '',
	'enablePrettyUrl' => true,
	'showScriptName' => false,
	'rules' => [

	],
];
<?php

use console\components\ClientMigration;

/**
 * Handles the creation of table `{{%clients}}`.
 */
class m190905_114751_create_client_table extends ClientMigration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%client}}', [
			'id' => $this->primaryKey(),
			'username' => $this->string()->notNull()->unique(),
			'email' => $this->string()->notNull()->unique(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
			'deleted' => $this->integer(1)->notNull(),
		], $tableOptions);
	}

	public function down()
	{
		$this->dropTable('{{%client}}');
	}
}

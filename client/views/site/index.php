<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'username',
			'email:email',
			'created_at',
			'updated_at',
			[
				'attribute' => 'deleted',
				'value' => function(\common\models\Client $data) { return $data->deleted ? "Yes" : "No";}
			],

			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>


</div>

<?php
namespace client\controllers;

use backend\models\search\ClientSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\httpclient\Client;
use yii\web\UrlManager;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$searchModel = new ClientSearch();
		$dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
    }

	/**
	 * Using yii\httpclient\Client making http request
	 */
	public function actionSync()
	{
		$httpClient = new Client(); // yii\httpclient\Client NOT \common\models\Client
		/**
		 * @var UrlManager $apiUrlManager
		 */
		$apiUrlManager = \Yii::$app->apiUrlManager;
		// Getting last updated client
		$client = \client\models\Client::find()->orderBy(['updated_at' => SORT_DESC])->one();

		$arrayParams = ['filter[updated_at][gt]' => $client ? $client->updated_at : 0];
		$params = array_merge(["clients"], $arrayParams);

		$response = $httpClient->createRequest()
			->setMethod('GET')
			->setFormat(Client::FORMAT_JSON)
			->setUrl($apiUrlManager->createAbsoluteUrl($params))
			->send();

		if ($response->isOk) {
			// TODO: Better to move this to the service class
			$clients = ArrayHelper::index($response->getData(), 'id');
			$ids = ArrayHelper::getColumn($clients,'id');
			$existed_clients = \client\models\Client::find()->andWhere(['in','id',$ids])->all();

			foreach ($existed_clients as $client){
				$client->setAttributes($clients[$client->id]);
				$client->save(false);
				unset($clients[$client->id]);
			}

			foreach ($clients as $client){
				$newClient = new \client\models\Client();
				$newClient->setAttributes($client);
				$newClient->save();
			}
			\Yii::$app->session->setFlash('success','Clients were synchronized successfully');
		} else {
			\Yii::$app->session->setFlash('error','Clients were not synchronized');
		}
		$this->redirect(['site/index']);
    }

}

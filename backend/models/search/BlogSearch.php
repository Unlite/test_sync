<?php

namespace backend\models\search;

use common\models\query\CategoryQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Blog;

/**
 * BlogSearch represents the model behind the search form of `\common\models\Blog`.
 */
class BlogSearch extends Blog
{
	public $category_slug;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','category_id'], 'integer'],
            [['category_slug'],'string'],
            [['title', 'short_description', 'description', 'slug', 'category_slug'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Blog::find();

        // add conditions that should always apply here
		if($this->category_slug){
			$category_slag = $this->category_slug;
			$items = explode('/',$this->category_slug);
			if(count($items) > 1){
				$category_slag = $items[array_key_last($items)];
			}
			$query->joinWith(['category' => function(CategoryQuery $query) use ($category_slag){
				return $query->andWhere(['category.slug' => $category_slag]);
			}]);
		}

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category.id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}

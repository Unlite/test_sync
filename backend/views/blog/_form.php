<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(
        [''=> 'Select category'] + Category::dropDownList('notroot'),
        ['options' => Category::disabledDropDownList()]
    ) ?>

    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <label class="control-label" for="blog-slug">Slug</label>
	<?= $form->field($model, 'slug',['options' => ['class' => 'form-group input-group'],'template'=>'{label}{input}                <div class="input-group-btn">
                    <a class="btn btn-primary" id="generate-slug">Auto</a>
                </div>'])->textInput(['class'=>'form-control','id' => 'slug'])->label(false)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs(<<<JS
    $(document).on('click','#generate-slug',function(e) {
      e.preventDefault();
      $.ajax({
      url:'/blog/slug' + '?string=' + encodeURIComponent($('#blog-title').val()),
      dataType:'json',
      success:function(result) {
        $('#slug').val(result.slug)
      }
      })
    })
JS
);?>
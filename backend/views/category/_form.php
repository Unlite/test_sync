<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'parent_id')->dropDownList(Category::dropDownList()); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <label class="control-label" for="category-slug">Slug</label>
	<?= $form->field($model, 'slug',['options' => ['class' => 'form-group input-group'],'template'=>'{label}{input}                <div class="input-group-btn">
                    <a class="btn btn-primary" id="generate-slug">Auto</a>
                </div>'])->textInput(['class'=>'form-control','id' => 'slug'])->label(false)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs(<<<JS
    $(document).on('click','#generate-slug',function(e) {
      e.preventDefault();
      $.ajax({
      url:'/category/slug' + '?string=' + encodeURIComponent($('#category-name').val()),
      dataType:'json',
      success:function(result) {
        $('#slug').val(result.slug)
      }
      })
    })
JS
);?>

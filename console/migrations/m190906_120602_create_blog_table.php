<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%blog}}`.
 */
class m190906_120602_create_blog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blog}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(11)->notNull(),
            'title' => $this->string(1024)->notNull(),
            'short_description' => $this->string(2048),
			'created_at' => $this->integer(10)->notNull(),
			'updated_at' => $this->integer(10)->notNull(),
            'description' => $this->text(),
            'slug' => $this->string(255),
        ], $tableOptions);

		// creates index for column `site_id`
		$this->createIndex(
			'idx-blog-category_id',
			'{{%blog}}',
			'category_id'
		);

		// add foreign key for table `site`
		$this->addForeignKey(
			'fk-blog-category_id',
			'{{%blog}}',
			'category_id',
			'{{%category}}',
			'id',
			'NO ACTION'
		);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropTable('{{%blog}}');
    }
}

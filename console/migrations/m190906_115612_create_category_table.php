<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m190906_115612_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('category', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255)->notNull(),
			'slug' => $this->string(255)->notNull(),
			'lft' => $this->integer(11)->notNull(),
			'rgt' => $this->integer(11)->notNull(),
			'depth' => $this->integer(11)->notNull(),
		],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category}}');
    }
}

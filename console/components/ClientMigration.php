<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 07.09.2019
 * Time: 00:08:12
 */

namespace console\components;


use yii\db\Migration;

class ClientMigration extends Migration
{
	public function init()
	{
		$this->db = 'db-client';
		parent::init();
	}
}
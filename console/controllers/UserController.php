<?php

namespace console\controllers;


use common\models\form\SignUpForm;
use common\models\User;
use common\services\UserService;
use yii\base\Module;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class UserController extends Controller
{

	private $service;

	public function __construct($id, Module $module, UserService $service, array $config = [])
	{
		$this->service = $service;
		parent::__construct($id, $module, $config);
	}

    /**
     * Help method
     */
    public function actionIndex()
    {
        $command = $this->ansiFormat('yii user/', Console::FG_YELLOW);
        $actionCreate = $this->ansiFormat('create', Console::FG_GREEN);
        $this->stdout("{$actionCreate} \t -> \t Create new user. {$command}{$actionCreate} [name] [email] [password]" . PHP_EOL);
        $actionDelete = $this->ansiFormat('delete', Console::FG_GREEN);
        $this->stdout("{$actionDelete} \t -> \t Delete user. {$command}{$actionDelete} [username]" . PHP_EOL);
    }


    /**
     * Create new user. Arguments: [username] [email] [password]
     * @param string|null $username
     * @param string|null $email
     * @param string|null $password
     * @return int
     */
    public function actionCreate($username = null, $email = null, $password = null, $day = null, $month = null, $year = null)
    {
        $form = new SignUpForm([
            'username' => $username ?: $this->prompt('Username:', ['required' => true]),
            'email' => $email ?: $this->prompt('Email:', ['required' => true]),
            'password' => $password ?: $this->prompt('Password:', ['required' => true]),
        ]);
        $user = $this->service->requestSignup($form);
        if (!$user) {
            foreach ($form->getFirstErrors() as $attribute => $error) {
                $this->stderr("$attribute: $error" . PHP_EOL);
            }
            return ExitCode::UNSPECIFIED_ERROR;
        }

        return ExitCode::OK;
    }

    /**
     * Hard delete user. Arguments: [username]
     * @param string|null $username
     * @return int
     */
    public function actionDelete($username = null)
    {
        if ($username)
            $user = $this->findUser($username, 'username');
        else
            $user = $this->findUser();
        if (!$user) {
            $this->stderr('User not found.' . PHP_EOL);
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $username = $this->ansiFormat($user->username, Console::FG_YELLOW);
        if ($this->confirm("Are you sure you want to delete the user {$username}?")) {
            if ($this->service->userDelete($user)) {
                $this->stdout("User {$username} deleted!" . PHP_EOL);
                return ExitCode::OK;
            } else {
                $this->stdout("User {$username} can't be deleted!" . PHP_EOL);
                return ExitCode::UNSPECIFIED_ERROR;
            }
        }
        return ExitCode::OK;
    }


	/**
	 * @param string|null $searchValue
	 * @param string|null $method
	 * @return null|User
	 */
	private function findUser($searchValue = null, $method = null)
	{
		$methods = ['username', 'email', 'id'];
		if (!$method || !in_array($method, $methods)) {
			$this->stdout('Choose search method:' . PHP_EOL);
			$this->stdout('    -> 0 - by username' . PHP_EOL);
			$this->stdout('    -> 1 - by email' . PHP_EOL);
			$this->stdout('    -> 2 - by id' . PHP_EOL);
		}
		if (!$searchValue)
			$searchValue = $this->prompt(lcfirst($method) . ':', ['required' => true]);

		return User::findOne([$method => $searchValue]);
	}
}
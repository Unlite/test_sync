<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 07.09.2019
 * Time: 00:12:34
 */

namespace console\controllers;

use yii\console\controllers\MigrateController;

class ClientMigrationController extends MigrateController
{
	public $db = 'db-client';
	public $templateFile = '@app/console/views/migrations/migration_db_client.php';
	public $migrationPath = 'client/migrations/';
}
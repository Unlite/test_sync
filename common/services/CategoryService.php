<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 06.07.2019
 * Time: 14:06:13
 */

namespace common\services;

use common\models\Category;

class CategoryService
{
	public static function createCategory (Category &$model) {
		// if category has parent category, add category as child to parent
		if($model->parent_id && Category::find()->roots()->count()){
			$model->appendTo(Category::findOne(['id'=>$model->parent_id]));
		} else {
			// if category has NO parent category, set category as root category OR add to root
			if(Category::find()->roots()->count()){
				$root = Category::find()->roots()->one();
				$model->appendTo($root);
			} else {
				$model->makeRoot();
			}
		}
	}
}
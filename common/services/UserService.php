<?php

namespace common\services;

use common\models\form\SignUpForm;
use common\models\User;

class UserService
{
    public function requestSignup(SignUpForm $form)
    {
        if ($form->validate()) {
            $user = User::requestSignup(
                $form->username,
                $form->email,
                $form->password
            );
            if ($user->save(false)) {
                return $user;
            }else{
                throw new \RuntimeException('Can\'t save user.');
            }
        }
        return null;
    }

    public function userDelete(User $user)
    {
		return $user->delete();
    }
}
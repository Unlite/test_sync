<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
	'controllerMap' => [
		'migrate' => [ // Fixture generation command line.
			'class' => 'yii\console\controllers\MigrateController',
			'db' => 'db',
			'templateFile' => '@app/views/migrations/migration_db.php',
			'migrationPath' => '@app/migrations/db'
		],
		'migrate-client' => [ // Fixture generation command line.
			'class' => 'console\controllers\ClientMigrationController',
		],
    ],
];

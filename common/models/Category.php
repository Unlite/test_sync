<?php

namespace common\models;

use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 */
class Category extends \yii\db\ActiveRecord
{
	private $_parent_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['lft', 'rgt', 'depth'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
			[['parent_id'],'safe']
        ];
    }

	public function behaviors() {
		return [
			'tree' => [
				'class' => NestedSetsBehavior::className(),
			],
		];
	}

	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_ALL,
		];
	}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
        ];
    }

	public static function dropDownList($tip = NULL)
	{
		if($tip === NULL){
			$cats=Category::find()->orderBy(array('lft'=>'ASC'))->all();
		}
		if($tip=="root"){
			$cats = Category::find()->roots()->all();
		}
		if($tip=="onlyroot"){
			$catsRoot=Category::find()->roots()->find()->lft();
			$cats=$catsRoot->children()->all();
		}
		if($tip=="woroot"){
			$cats=Category::find()->lft()->notroot()->all();
		}
		if($tip === 'notroot'){
			$cats=Category::find()->lft()->notroot()->all();
		}
		if($tip === 'leaves'){
			$cats=Category::find()->lft()->leaves()->all();
		}
		$list=array();
		if (count($cats)>0) {

			if($tip=="onlyroot") $list[1]='root';
			foreach ($cats as $cat){
				if($cat->name == 'root') $cat->name = "----";
				$list[$cat->id]=str_repeat('• ',$cat->depth).$cat->name;
			}
		}

		return $list;
	}

	public static function disabledDropDownList($tip = NULL)
	{
		$disabled = [];

		if($tip === NULL){
			$cats=Category::find()->notroot()->lft()->all();
			foreach ($cats as $cat){
				if(!$cat->isLeaf())
					$disabled[$cat->id] = ['disabled' => true];
			}
		}

		return $disabled;
	}

	/**
	 * Returns full category tree
	 * @param string $separator
	 * @return string
	 */
	public function renderCategoryTree($separator = ' / ')
	{
		$parents = ArrayHelper::getColumn($this->parents()->all(),'name');
		if(count($parents) > 0)
			unset($parents[0]);
		$string = implode($separator,$parents);
		$string .= count($parents) > 0 ? $separator.$this->name : ''.$this->name;
		return $string;
	}

	/**
	 * Returns full category tree but using slugs
	 * @param string $separator
	 * @return string
	 */
	public function renderCategorySlugTree($separator = '/')
	{
		$parents = ArrayHelper::getColumn($this->parents()->all(),'slug');
		if(count($parents) > 0)
			unset($parents[0]);
		$string = implode($separator,$parents);
		$string .= count($parents) > 0 ? $separator.$this->slug : ''.$this->slug;
		return $string;

	}

	public function getParent_id()
	{
		return $this->_parent_id;
	}

	public function setParent_id($parent_id)
	{
		$this->_parent_id = $parent_id;
	}

    /**
     * {@inheritdoc}
     * @return \common\models\query\CategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\CategoryQuery(get_called_class());
    }
}

<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "blog".
 *
 * @property int $id
 * @property integer $category_id
 * @property string $title
 * @property string $short_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $description
 * @property string $slug
 *
 * @property Category $category
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','category_id','slug'], 'required'],
			[['created_at', 'updated_at','category_id'], 'integer'],
			[['title', 'slug'], 'trim'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 1024],
			[['slug'], 'match', 'pattern' => '/^[a-z0-9]+(?:-[a-z0-9]+)*$/i'],
			[['slug'], 'unique'],
            [['short_description'], 'string', 'max' => 2048],
            [['slug'], 'string', 'max' => 255],
			[['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
		];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
            'description' => 'Description',
            'slug' => 'Slug',
        ];
    }

	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
	}

	/**
	 * Relation to category table
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory()
	{
		return $this->hasOne(Category::class, ['id' => 'category_id']);
	}

    /**
     * {@inheritdoc}
     * @return \common\models\query\BlogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\BlogQuery(get_called_class());
    }
}

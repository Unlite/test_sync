<?php

namespace common\models\query;

use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * This is the ActiveQuery class for [[\common\models\Category]].
 *
 * @see \common\models\Category
 */
class CategoryQuery extends \yii\db\ActiveQuery
{
	public function behaviors() {
		return [
			NestedSetsQueryBehavior::className(),
		];
	}

	public function lft()
	{
		return $this->orderBy('lft');
	}

	public function rgt()
	{
		return $this->orderBy('rgt');
	}

	public function notroot()
	{
		return $this->where(['>=','depth','1']);
	}

    /**
     * {@inheritdoc}
     * @return \common\models\Category[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Category|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

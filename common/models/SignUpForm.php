<?php
namespace common\models;

use yii\base\Model;
use common\models\User;

/**
 * SignUp form
 */
class SignUpForm extends Model
{
	public $username;
	public $email;
	public $password;


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['username', 'trim'],
			['username', 'required'],
			['username', 'match', 'pattern' => '#^[\w_-]+$#is'],
			['username', 'unique', 'targetClass' => User::class, 'message' => 'This username has already been taken.'],
			['username', 'string', 'min' => 2, 'max' => 255],

			['email', 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique', 'targetClass' => User::class, 'message' => 'This email address has already been taken.'],

			['password', 'required'],
			['password', 'string', 'min' => 6],
		];
	}
}

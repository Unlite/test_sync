**Backend** application used for simple CRUD for categories / blogs <br>
Also CURD Clients for "Server" function

**Client** application is using different DB, imitating "Remote client" 

**Api** application is used for "Synchronizing" users

**Frontend** application for displaying blogs & categories, SEO nested links etc.

I've used native Yii2 rest **ActiveController** for api. <br>
The **Client** is synchronizing data by updated_at column in "Client" table.<br>
Just by compare latest **update_at** row in the databases we can easily synchronize databases.<br>
I've used soft delete for "Client" table, to make it easier to synchronize clients.
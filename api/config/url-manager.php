<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 08.09.2019
 * Time: 18:13:25
 */

return [
	'class' => 'yii\web\UrlManager',
	'hostInfo' => $params['apiHostInfo'],
	'baseUrl' => '',
	'enablePrettyUrl' => true,
	'showScriptName' => false,
	'enableStrictParsing' => true,
	'rules' => [
		['class' => 'yii\rest\UrlRule', 'controller' => 'client'],
	],
];
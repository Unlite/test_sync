<?php

namespace api\controllers;

use yii\rest\ActiveController;

class ClientController extends ActiveController
{
	public $modelClass = 'common\models\Client';

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		$actions = parent::actions();

		$actions['index']['dataFilter'] = [
			'class' => 'yii\data\ActiveDataFilter',
			'searchModel' => 'backend\models\search\ClientSearch'
		];

		return $actions;
	}

}

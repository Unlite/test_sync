<?php
namespace frontend\controllers;

use backend\models\search\BlogSearch;
use common\models\Blog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * Displays index page of blogs.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$searchModel = new BlogSearch();
		$dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
    }

    /**
     * Displays view page of blog.
     * @param string $category_slug
     * @param string $blog_slug
     * @return mixed
	 * @throws NotFoundHttpException
     */
    public function actionBlog($category_slug = null,$blog_slug)
    {
    	$model = Blog::find()->andWhere(['slug' => $blog_slug])->one();

    	if($model)
			return $this->render('/blog/view',['model' => $model]);
    	else
    		throw new NotFoundHttpException();
    }

    /**
     * Displays blog in category.
	 * @param string $category_slug
	 * @return mixed
     */
    public function actionCategory($category_slug)
    {
		$searchModel = new BlogSearch(['category_slug' => $category_slug]);
		$dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
    }

}

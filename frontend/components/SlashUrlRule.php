<?php
namespace frontend\components;
use yii\web\UrlRuleInterface;

/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 06.09.2019
 * Time: 19:30:33
 */

class SlashUrlRule implements UrlRuleInterface {


	public function createUrl($manager, $route, $params)
	{
		$items = explode('/',$route);
		if($items[0] == 'site'){
			if($items[1] == 'category'){
				return $items[1] .'/'. $this->fixPathSlashes(join('/',$params));
			}
			if($items[1] == 'blog'){
				return 'news/'. $this->fixPathSlashes(join('/',$params));
			}
		}
		return false;
	}

	/**
	 * @param \yii\web\UrlManager $manager
	 * @param \yii\web\Request $request
	 * @return bool
	 */
	public function parseRequest($manager, $request)
	{
		return false;
	}

	protected  function fixPathSlashes($url)
	{
		return preg_replace('|\%2F|i', '/', $url);
	}
}
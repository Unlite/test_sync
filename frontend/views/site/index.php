<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\search\BlogSearch */

$this->title = 'My Yii Application';
?>
<div class="container">
    <div class="col-md-4">
        <h2 class="text-center">
            Categories
        </h2>
        <?=$this->render('/category/index')?>
    </div>
    <div class="col-md-8">
        <h2 class="text-center">
            Blogs
        </h2>
        <?=$this->render('/blog/index',['dataProvider' => $dataProvider,'searchModel' => $searchModel])?>
    </div>
</div>

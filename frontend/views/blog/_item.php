<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 06.09.2019
 * Time: 18:33:01
 * @var $model \common\models\Blog
 */

?>
<div class="col-xs-6">
	<div class="panel panel-default">
		<div class="panel-heading">
            <div class="pull-left">
                <?=\yii\helpers\Html::a($model->title,['site/blog','category_slug' => $model->category->renderCategorySlugTree(), 'blog_slug' => $model->slug])?>
            </div>
            <div class="pull-right">
				<?=(new DateTimeImmutable())->setTimestamp($model->created_at)->format('d.m.Y')?>
            </div>
            <div class="clearfix"></div>
        </div>
		<div class="panel-body"><?=$model->short_description?></div>
	</div>
</div>
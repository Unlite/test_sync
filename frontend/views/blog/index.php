<?php
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\search\BlogSearch */

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '/blog/_item',
    'emptyText' => "",
    'summary' => '',
    'layout' => '{items}'
]);

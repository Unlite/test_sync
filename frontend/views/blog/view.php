<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 06.09.2019
 * Time: 18:33:01
 * @var $model \common\models\Blog
 */

?>

<div class="container">
    <div class="col-md-4">
        <h2 class="text-center">
            Categories
        </h2>
		<?=$this->render('/category/index')?>
    </div>
    <div class="col-md-8">
        <h2 class="text-center">
            Blog
        </h2>
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="pull-left">
                    <?=\yii\helpers\Html::a($model->title,['site/blog','slug' => $model->slug])?>
                </div>
                <div class="pull-right">
                    <?=(new DateTimeImmutable())->setTimestamp($model->created_at)->format('d.m.Y')?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body"><?=$model->description?></div>
        </div>
    </div>
</div>
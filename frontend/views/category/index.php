<?php
use yii\widgets\ListView;
/* @var $this yii\web\View */

?>
    <?= ListView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => \common\models\Category::find()->lft()->notroot()
        ]),
        'itemView' => '/category/_item',
        'emptyText' => "",
        'summary' => '',
        'layout' => '{items}'
    ]);
    ?>

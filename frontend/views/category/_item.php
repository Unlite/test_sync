<?php
/**
 * Created by PhpStorm.
 * User: Freelancer
 * Date: 06.09.2019
 * Time: 18:46:05
 * @var $model \common\models\Category
 */

echo \yii\helpers\Html::a(str_repeat('• ',$model->depth).$model->name,['site/category','category_slug' => $model->renderCategorySlugTree()])?>

